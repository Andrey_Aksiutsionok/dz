﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            IWebDriver driver = new ChromeDriver();
           // IWebDriver driver = new FirefoxDriver();
            driver.Url = "https://www.kinopoisk.ru/";
            Console.WriteLine(driver.Title);
            IWebElement searchField = driver.FindElement(By.XPath("//form/input[@name = 'kp_query']"));
            searchField.SendKeys("тест");
            IWebElement searchValue = driver.FindElement(By.XPath("//input[@title='перейти к первому результату поиска']"));
            searchValue.Click();
            driver.Navigate().GoToUrl("http://www.seleniumhq.org");
            driver.Navigate().Back();
            driver.Close();           
        }
    }
}
